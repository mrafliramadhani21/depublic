# Depublic Ticketing Api

## Overview

- Service API mengenai Pembelian Tiket Konser/Event dengan Bahasa Go

## Prerequisites

Sebelum mulai menjalankan project nya, harus menginstal beberapa tools dibawah ini.
- Go , install dari [golang.org](https://golang.org/dl/).
- Golang Migrate, instal dari [pkg.go.dev](https://pkg.go.dev/github.com/golang-migrate/migrate/cli).
- Redis server. lihat cara install di docker : [cara install redis](https://medium.com/@praveenr801/introduction-to-redis-cache-using-docker-container-2e4e2969ed3f).
- Postgresql, lihat cara install Postgresql di docker : [cara install postgres](https://www.dbvis.com/thetable/how-to-set-up-postgres-using-docker/).
- Make (tool untuk menjalankan command makefile), [cara install](https://medium.com/@samsorrahman/how-to-run-a-makefile-in-windows-b4d115d7c516).

## Installation

- Clone repository ini dengan :

    ```sh
    git clone https://gitlab.com/mrafliramadhani21/depublic.git
    cd Depublic
    ```
    

- Copy .env.example dan ubah namanya menjadi .env :

    ```sh
    cp .env.example .env
    ```
    
- konfigurasi .env sesuai kredensial Postgresql dan Redisnya, untuk jwt dan encrypt bisa di biarkan default atau di ubah (*ENCRYPT_SECRET_KEY, dan ENCRYPT_IV HARUS 16 karakter)

- Download package golang yang dibutuhkan dengan :

    ```sh
    go mod tidy
    ```
    
- Jalankan Postgresql, dan Redis server :
    ```sh
    sudo docker start [container_name]
    ```

- Gunakan Database Client untuk Postgresql :
    - bisa gunakan list yang di bawah ini
        - Dbeaver
        - pgAdmin
        - NaviCat

- Jalankan migrasi dengan make tool :

    ```sh
    make migrate-up
    ```
    
- Jalankan server echo untuk memulai develop
    - bisa dengan make command
    ```sh
    make run-server
    ```

    - atau dengan
    ```sh
    go run cmd/app/main.go
    ```

## Features in Depublic App

- User Registration.
- Implementation of In-App Notification (in-app notification, not push notification).
- User Profile.
- Transaction History.
- Search, Sort, and Filter.
- Realtime Payment by Midtrans

## Tech

Project ini menggunakan technology berikut :

1. **Golang** - High-performance language for scalable apps.
2. **Echo Framework** - Web framework for Go.
3. **PostgreSQL** - Reliable open-source database.
4. **Midtrans** - Secure online payment gateway.
5. **MailerSend** - Sending Email

## Available Roles

- Admin
- Buyer

## API Documentation

Using Postman

```sh
https://documenter.getpostman.com/view/20610152/2sA3XWdzJd
or
https://gitlab.com/mrafliramadhani21/depublic/-/blob/main/depublic.postman_collection.json
```
## Our Teams

Project ini dikembangkan oleh 5 orang :
| Name | Image | Gitlab |
| ------ | ------ | ------ |
| Rafli | ![](images/teams/rafli.png) | https://gitlab.com/mrafliramadhani21
| Tama | ![](images/teams/tama.png) | https://gitlab.com/rezaprtmmm
| Rizki | ![](images/teams/rizki.png) | https://gitlab.com/rizkiadiansyahh
| Nanda | ![](images/teams/nanda.png) | https://gitlab.com/annisananda2902
| Echa | ![](images/teams/echa.png) | https://gitlab.com/anantaechaa20