package main

import (
	"time"

	"gitlab.com/mrafliramadhani21/depublic/configs"
	"gitlab.com/mrafliramadhani21/depublic/internal/builder"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gitlab.com/mrafliramadhani21/depublic/pkg/encrypt"
	"gitlab.com/mrafliramadhani21/depublic/pkg/postgres"
	"gitlab.com/mrafliramadhani21/depublic/pkg/server"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
)

func main() {
	cfg, err := configs.LoadConfig(".env")
	checkError(err)

	go service.StartEmailProcessor()

	db, err := postgres.InitPostgres(&cfg.Postgres)
	checkError(err)

	redisDB := cache.InitCache(&cfg.Redis)
	tokenUseCase := token.NewTokenUseCase(cfg.JWT.SecretKey, time.Duration(cfg.JWT.ExpiredIn)*time.Hour)
	encryptTool := encrypt.NewEncryptTool(cfg.Encrypt.SecretKey, cfg.Encrypt.IV)

	publicRoutes := builder.BuildAppPublicRoutes(db, cfg, tokenUseCase, encryptTool)
	privateRoutes := builder.BuildAppPrivateRoutes(db, cfg, redisDB, encryptTool)

	srv := server.NewServer("app", publicRoutes, privateRoutes, cfg.JWT.SecretKey)
	srv.Run()
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
