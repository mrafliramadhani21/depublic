package main

import (
	"gitlab.com/mrafliramadhani21/depublic/internal/builder"
	"gitlab.com/mrafliramadhani21/depublic/pkg/server"
)

func main() {
	publicRoutes := builder.BuildAuthPublicRoutes()
	privateRoutes := builder.BuildAuthPrivateRoutes()

	srv := server.NewServer("auth", publicRoutes, privateRoutes, "")
	srv.Run()
}
