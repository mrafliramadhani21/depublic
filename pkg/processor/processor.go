package processor

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/gocraft/work"
	"github.com/gomodule/redigo/redis"
	"gopkg.in/gomail.v2"

	"gitlab.com/mrafliramadhani21/depublic/configs"
)

func Init() {
	cfg, err := configs.LoadConfig(".env")
	checkError(err)

	var redisPool = &redis.Pool{
		MaxActive: 5,
		MaxIdle:   5,
		Wait:      true,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%s:%s", cfg.Redis.Host, cfg.Redis.Port))
		},
	}

	pool := work.NewWorkerPool(Context{}, 10, cfg.Namespace, redisPool)

	pool.Middleware((*Context).Log)
	pool.Middleware((*Context).FindCustomer)

	pool.Job("send_email_verification", (*Context).SendEmail)

	pool.Start()

	signalChan := make(chan os.Signal, 1)

	signal.Notify(signalChan, os.Interrupt, os.Kill)
	<-signalChan

	pool.Stop()
}

type Context struct {
	email  string
	userID int64
}

func (c *Context) Log(job *work.Job, next work.NextMiddlewareFunc) error {
	fmt.Println("Starting Job: ", job.Name)
	return next()
}

func (c *Context) FindCustomer(job *work.Job, next work.NextMiddlewareFunc) error {
	fmt.Println("Finding customer: ", c.email)
	// if _, ok := job.Args["user_id"]; !ok {
	// 	c.userID = job.ArgInt64("user_id")
	// 	c.email = job.ArgString("email_address")
	// 	if err := job.ArgError(); err != nil {
	// 		return fmt.Errorf("arg error %v", err.Error())
	// 	}
	// }
	return next()
}

func (c *Context) SendEmail(job *work.Job) error {
	return SendEmail(
		job.ArgString("email_address"),
		job.ArgString("subject"),
		job.ArgString("body"),
	)
}

func SendEmail(to, subject, body string) error {
	cfg, err := configs.LoadConfig(".env")
	checkError(err)

	from := cfg.SMTP.From
	username := cfg.SMTP.Username
	password := cfg.SMTP.Password
	smtpHost := cfg.SMTP.Host
	smtpPort := 587

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", from)
	mailer.SetHeader("To", to)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", body)

	dialer := gomail.NewDialer(smtpHost, smtpPort, username, password)

	err = dialer.DialAndSend(mailer)
	checkError(err)

	fmt.Println("Email sent successfully to:", to)
	return nil
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
