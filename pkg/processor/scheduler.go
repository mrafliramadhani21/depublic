package processor

import (
	"fmt"
	"log"

	"github.com/gocraft/work"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/mrafliramadhani21/depublic/configs"
)

func ScheduleEmails(
	emailAddresses []string,
	subject string,
	body string,
) {
	cfg, err := configs.LoadConfig(".env")
	checkError(err)
	fmt.Println(cfg.Namespace)

	var redisPool = &redis.Pool{
		MaxActive: 5,
		MaxIdle:   5,
		Wait:      true,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%s:%s", cfg.Redis.Host, cfg.Redis.Port))
		},
	}

	var enqueuer = work.NewEnqueuer(cfg.Namespace, redisPool)

	for _, email := range emailAddresses {
		_, err = enqueuer.Enqueue("send_email_verification", work.Q{"email_address": email, "subject": subject, "body": body})
		if err != nil {
			log.Fatal(err)
		}
	}
}
