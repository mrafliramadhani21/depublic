package mail

import "gopkg.in/gomail.v2"

type IMail interface {
	SendEmail(to string, subject string, body string) error
}

type mailCtx struct {
	host     string
	username string
	port     int
	password string
}

func NewMail(host string,
	username string,
	port int,
	password string) IMail {
	return &mailCtx{
		host:     host,
		username: username,
		password: password,
		port:     port,
	}
}

func (m *mailCtx) SendEmail(to string, subject string, body string) error {

	msg := gomail.NewMessage()
	msg.SetHeader("From", m.username)
	msg.SetHeader("To", to)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/html", body)

	n := gomail.NewDialer(m.host, m.port, m.username, m.password)
	if err := n.DialAndSend(msg); err != nil {
		return err
	}

	return nil
}
