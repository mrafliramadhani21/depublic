package token

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type TokenUseCase interface {
	GenerateAccessToken(claims JwtCustomClaims) (string string, expiredAt time.Time, err error)
}

type tokenUseCase struct {
	secretKey   string
	expiredTime time.Duration
}

type JwtCustomClaims struct {
	ID          string `json:"id"`
	Email       string `json:"email"`
	Role        string `json:"role"`
	PhoneNumber string `json:"phone_number"`
	jwt.RegisteredClaims
}

func NewTokenUseCase(secretKey string, expiredTime time.Duration) *tokenUseCase {
	return &tokenUseCase{
		secretKey:   secretKey,
		expiredTime: expiredTime,
	}
}

func (t *tokenUseCase) GenerateAccessToken(claims JwtCustomClaims) (string string, expiredAt time.Time, err error) {

	expiredTime := time.Now().Add(t.expiredTime)
	claims.ExpiresAt = jwt.NewNumericDate(expiredTime)

	plainToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	encodedToken, err := plainToken.SignedString([]byte(t.secretKey))

	if err != nil {
		return "", time.Time{}, err
	}

	return encodedToken, claims.ExpiresAt.Time, nil
}
