package service

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
	"gitlab.com/mrafliramadhani21/depublic/pkg/encrypt"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	Login(email string, password string) (string, time.Time, error)
	VerifyEmail(id uuid.UUID) error
	FindAllUser() ([]entity.User, error)
	CreateUser(user *entity.User) (*entity.User, error)
	Register(user *entity.User) (*entity.User, error)
	UpdateUser(user *entity.User) (*entity.User, error)
	DeleteUser(id uuid.UUID) (bool, error)
	FindUserByID(id uuid.UUID) (*entity.User, error)
}

type userService struct {
	userRepository repository.UserRepository
	tokenUseCase   token.TokenUseCase
	encryptTool    encrypt.EncryptTool
}

func NewUserService(userRepository repository.UserRepository, tokenUseCase token.TokenUseCase, encryptTool encrypt.EncryptTool) UserService {
	return &userService{
		userRepository: userRepository,
		tokenUseCase:   tokenUseCase,
		encryptTool:    encryptTool,
	}
}

func (s *userService) Login(email string, password string) (string, time.Time, error) {
	user, err := s.userRepository.FindUserByEmail(email)
	if err != nil {
		return "", time.Time{}, errors.New("email/password yang anda masukkan salah")
	} else {
		if !user.IsVerified {
			return "", time.Time{}, errors.New("email belum diverifikasi")
		}
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", time.Time{}, errors.New("email/password yang anda masukkan salah")
	}

	user.PhoneNumber, _ = s.encryptTool.Decrypt(user.PhoneNumber)

	claims := token.JwtCustomClaims{
		ID:          user.ID.String(),
		Email:       user.Email,
		Role:        user.Role,
		PhoneNumber: user.PhoneNumber,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer: "Depublic",
		},
	}

	token, expiredAt, err := s.tokenUseCase.GenerateAccessToken(claims)
	if err != nil {
		return "", time.Time{}, err
	}

	return token, expiredAt, nil
}

func (s *userService) FindAllUser() ([]entity.User, error) {
	users, err := s.userRepository.FindAllUser()
	if err != nil {
		return nil, err
	}

	formattedUser := make([]entity.User, 0)
	for _, v := range users {
		v.PhoneNumber, _ = s.encryptTool.Decrypt(v.PhoneNumber)
		formattedUser = append(formattedUser, v)
	}

	return formattedUser, nil
}

func (s *userService) CreateUser(user *entity.User) (*entity.User, error) {
	_, err := s.userRepository.FindUserByEmail(user.Email)
	if err == nil {
		return nil, errors.New("email sudah terdaftar")
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	user.Password = string(hashedPassword)
	user.Role = "Admin"

	newUser, err := s.userRepository.CreateUser(user)

	if err != nil {
		return nil, err
	}

	return newUser, nil
}

func (s *userService) Register(user *entity.User) (*entity.User, error) {
	_, err := s.userRepository.FindUserByEmail(user.Email)
	if err == nil {
		return nil, errors.New("email sudah terdaftar")
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	user.Password = string(hashedPassword)
	user.Role = "Buyer"

	newUser, err := s.userRepository.CreateUser(user)
	emailAddresses := []string{newUser.Email}

	verificationURL := fmt.Sprintf("http://localhost:8000/app/api/v1/verify/%s", newUser.ID.String())
	html := CreateVerificationHTML(verificationURL, newUser.Fullname)
	ScheduleEmails(
		emailAddresses,
		"Please Verify Your Email Address",
		html,
	)

	if err != nil {
		return nil, err
	}

	return newUser, nil
}

func (s *userService) VerifyEmail(id uuid.UUID) error {
	user, err := s.userRepository.FindUserByID(id)
	if err != nil {
		return err
	}

	_, err = s.userRepository.UpdateVerificationStatus(user)
	if err != nil {
		return err
	}

	return nil
}

func (s *userService) UpdateUser(user *entity.User) (*entity.User, error) {
	if user.Password != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		user.Password = string(hashedPassword)
	}
	if user.PhoneNumber != "" {
		user.PhoneNumber, _ = s.encryptTool.Encrypt(user.PhoneNumber)
	}
	return s.userRepository.UpdateUser(user)
}

func (s *userService) DeleteUser(id uuid.UUID) (bool, error) {
	user, err := s.userRepository.FindUserByID(id)
	if err != nil {
		return false, err
	}

	return s.userRepository.DeleteUser(user)
}

func (s *userService) FindUserByID(id uuid.UUID) (*entity.User, error) {
	return s.userRepository.FindUserByID(id)
}
