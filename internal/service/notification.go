package service

import (
	"errors"

	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
)

type NotificationService interface {
	GetByUser(id uuid.UUID) ([]entity.Notification, error)
	FindNotificationByID(id uuid.UUID) (*entity.Notification, error)
	UpdateNotification(id uuid.UUID, data *entity.Notification) (*entity.Notification, error)
}

type notificationService struct {
	notificationRepository repository.NotificationRepository
}

func NewNotificationServices(notificationRepository repository.NotificationRepository) NotificationService {
	return &notificationService{
		notificationRepository: notificationRepository,
	}
}

func (s *notificationService) UpdateNotification(id uuid.UUID, data *entity.Notification) (*entity.Notification, error) {

	getNotif, err := s.notificationRepository.FindNotificationByID(id)
	if err != nil {
		return &entity.Notification{}, err
	}

	if getNotif.ID == uuid.Nil {
		return &entity.Notification{}, errors.New("notification not found")
	}

	updateNotif, err := s.notificationRepository.UpdateNotification(id, data)
	if err != nil {
		return nil, err
	}
	return updateNotif, nil
}

func (s *notificationService) GetByUser(id uuid.UUID) ([]entity.Notification, error) {

	getNotif, err := s.notificationRepository.FindAllByUserID(id)
	if err != nil {
		return nil, errors.New("notification not found")
	}

	return getNotif, nil
}

func (s *notificationService) FindNotificationByID(id uuid.UUID) (*entity.Notification, error) {
	notif, err := s.notificationRepository.FindNotificationByID(id)
	if err != nil {
		return nil, errors.New("notification not found")
	}

	return notif, nil
}
