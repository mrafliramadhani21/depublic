package service

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
)

type EventService interface {
	// Existing methods
	GetEventCategories(page, pageSize int) ([]entity.Category, error)
	// BuyTicket(userID, eventID, categoryID uuid.UUID, quantity int) (*entity.Ticket, error)

	// New CRUD methods for CMS
	CreateEvent(event *entity.Event) (*entity.Event, error)
	GetEvents(page, pageSize int) ([]entity.Event, error)
	FindEventByID(eventID uuid.UUID) (*entity.Event, error)
	UpdateEvent(event *entity.Event) (*entity.Event, error)
	DeleteEvent(eventID uuid.UUID) error
	CreateCategory(category *entity.Category) (*entity.Category, error)
	GetCategories(page, pageSize int) ([]entity.Category, error)
	FindCategoriesById(categoryID uuid.UUID) (*entity.Category, error)
	UpdateCategory(category *entity.Category) (*entity.Category, error)
	DeleteCategory(categoryID uuid.UUID) error

	GetSearchEvent(name, location string) ([]entity.Event, error)
	GetEventsByDateRange(start, end time.Time) ([]entity.Event, error)
	GetEventsByPriceRange(minPrice, maxPrice float64) ([]entity.Event, error)
	// GetFilterEvent(dateRangeStart, dateRangeEnd string, minPriceStr, maxPriceStr float64) ([]entity.Category, error)
}

type eventService struct {
	eventRepository repository.EventRepository
}

func NewEventService(eventRepository repository.EventRepository) EventService {
	return &eventService{eventRepository: eventRepository}
}

func (s *eventService) GetEventCategories(page, pageSize int) ([]entity.Category, error) {
	return s.eventRepository.GetEventCategories(page, pageSize)
}

func (s *eventService) GetSearchEvent(name, location string) ([]entity.Event, error) {
	return s.eventRepository.GetSearchEvent(name, location)
}

func (s *eventService) GetEventsByDateRange(start, end time.Time) ([]entity.Event, error) {
	return s.eventRepository.GetEventsByDateRange(start, end)
}

func (s *eventService) GetEventsByPriceRange(minPrice, maxPrice float64) ([]entity.Event, error) {
	return s.eventRepository.GetEventsByPriceRange(minPrice, maxPrice)
}

func (s *eventService) CreateEvent(event *entity.Event) (*entity.Event, error) {
	return s.eventRepository.CreateEvent(event)
}

func (s *eventService) GetEvents(page, pageSize int) ([]entity.Event, error) {
	events, err := s.eventRepository.GetEvents(page, pageSize)
	if err != nil {
		return nil, err
	}
	return events, nil
}

func (s *eventService) FindEventByID(eventID uuid.UUID) (*entity.Event, error) {
	return s.eventRepository.FindEventByID(eventID)
}

func (s *eventService) UpdateEvent(event *entity.Event) (*entity.Event, error) {
	return s.eventRepository.UpdateEvent(event)
}

func (s *eventService) DeleteEvent(eventID uuid.UUID) error {
	return s.eventRepository.DeleteEvent(eventID)
}

func (s *eventService) CreateCategory(category *entity.Category) (*entity.Category, error) {
	_, err := s.eventRepository.FindCategoriesByEventId(category.EventID, category.Name)
	if err == nil {
		return nil, errors.New("data category sudah ada")
	}

	newCategory, err := s.eventRepository.CreateCategory(category)
	if err != nil {
		return nil, err
	}

	return newCategory, nil
}

func (s *eventService) GetCategories(page, pageSize int) ([]entity.Category, error) {
	categories, err := s.eventRepository.GetCategories(page, pageSize)
	if err != nil {
		return nil, err
	}
	return categories, nil
}

func (s *eventService) FindCategoriesById(categoryID uuid.UUID) (*entity.Category, error) {
	return s.eventRepository.FindCategoriesById(categoryID)
}

func (s *eventService) UpdateCategory(category *entity.Category) (*entity.Category, error) {
	return s.eventRepository.UpdateCategory(category)
}

func (s *eventService) DeleteCategory(categoryID uuid.UUID) error {
	return s.eventRepository.DeleteCategory(categoryID)
}
