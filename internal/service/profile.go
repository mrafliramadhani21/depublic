package service

import (
	"errors"

	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
	"gitlab.com/mrafliramadhani21/depublic/pkg/encrypt"
	"golang.org/x/crypto/bcrypt"
)

type ProfileService interface {
	FindUserByID(id string) (*entity.User, error)
	UpdateProfile(user *entity.User) (*entity.User, error)
}

type profileService struct {
	userRepository repository.UserRepository
	encryptTool    encrypt.EncryptTool
}

func NewProfileService(userRepository repository.UserRepository, encryptTool encrypt.EncryptTool) ProfileService {
	return &profileService{userRepository: userRepository}
}

func (s *profileService) FindUserByID(id string) (*entity.User, error) {
	useriID, err := uuid.Parse(id)
	if err != nil {
		return nil, errors.New("invalid user ID format")
	}
	user, err := s.userRepository.FindUserByID(useriID)
	if err != nil {
		return nil, errors.New("user not found")
	}
	return user, nil
}

func (s *profileService) UpdateProfile(user *entity.User) (*entity.User, error) {
	if user.Password != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		user.Password = string(hashedPassword)
	}

	updateUser, err := s.userRepository.UpdateProfile(user)
	if err != nil {
		return nil, errors.New("user not found")
	}
	return updateUser, nil
}
