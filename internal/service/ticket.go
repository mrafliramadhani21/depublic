package service

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/midtrans/midtrans-go"
	"github.com/midtrans/midtrans-go/snap"
	"gitlab.com/mrafliramadhani21/depublic/configs"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
)

type TicketService interface {
	CreateReceipt(receipt *entity.Receipt) (interface{}, error)
	GetTicketsByUserID(userID string) ([]entity.Ticket, error)
	GetReceiptByID(receiptID string) (*entity.GetReceiptByID, error)
	GetReceiptHistory(userID string) ([]entity.Receipt, error)
	UpdateTransactionStatus(data *entity.Receipt) error
}

type ticketService struct {
	ticketRepository repository.TicketRepository
	cfg              *configs.Config
}

func NewTicketService(ticketRepository repository.TicketRepository, cfg *configs.Config) TicketService {
	return &ticketService{ticketRepository: ticketRepository, cfg: cfg}
}

func (s *ticketService) CreateReceipt(receipt *entity.Receipt) (interface{}, error) {
	categories, err := s.ticketRepository.GetCategoryByID(receipt.CategoryID)
	if err != nil {
		return nil, err
	}

	receipt.Price = float64(int64(categories.Price) * int64(receipt.Quantity))
	if categories.Stock < int(receipt.Quantity) && categories.Stock != 0 {
		return nil, fmt.Errorf("hanya tersisa %d tiket lagi", categories.Stock)
	}

	if categories.Stock == 0 {
		return nil, errors.New("tiket sudah habis")
	}

	receipt, err = s.ticketRepository.CreateReceipt(receipt)
	if err != nil {
		return nil, err
	}

	users, err := s.ticketRepository.GetUser(receipt.UserID)
	if err != nil {
		return nil, err
	}

	snapClient := snap.Client{}
	snapClient.New(s.cfg.MidtransConfig.ServerKey, midtrans.Sandbox)

	snapRequest := &snap.Request{
		TransactionDetails: midtrans.TransactionDetails{
			OrderID:  receipt.ID,
			GrossAmt: int64(receipt.Price),
		},
		CreditCard: &snap.CreditCardDetails{
			Secure: true,
		},
		Items: &[]midtrans.ItemDetails{
			{
				ID:    receipt.ID,
				Name:  categories.Name,
				Price: int64(categories.Price),
				Qty:   int32(receipt.Quantity),
			},
		},
		CustomerDetail: &midtrans.CustomerDetails{
			FName: users.Fullname,
			LName: "",
			Email: users.Email,
			BillAddr: &midtrans.CustomerAddress{
				Address: "Indonesia",
			},
		},
	}

	snapResponse, _ := snapClient.CreateTransaction(snapRequest)
	response := map[string]interface{}{"status": "success", "link": snapResponse.RedirectURL, "transaction": receipt}

	// Update stock
	err = s.ticketRepository.UpdateCategoriesStock(receipt.CategoryID, int64(receipt.Quantity))
	if err != nil {
		return nil, err
	}

	// Create notification
	err = s.ticketRepository.CreateNotification(&entity.Notification{
		ID:      uuid.New(),
		UserID:  receipt.UserID,
		Subject: "Konfirmasi Pembayaran",
		IsRead:  false,
		Body:    "Silahkan selesaikan pembayaran anda",
	})
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (s *ticketService) UpdateTransactionStatus(receipt *entity.Receipt) error {
	tickets := receipt.Tickets
	receipt.Tickets = []entity.Ticket{}

	for _, ticket := range tickets {
		if err := s.ticketRepository.CreateTicket(&ticket); err != nil {
			return err
		}
	}

	email, err := s.ticketRepository.GetUser(receipt.UserID)
	if err != nil {
		return err
	}

	emailAddresses := []string{email.Email}
	html := CreatePaymentSuccessHTML()
	ScheduleEmails(
		emailAddresses,
		"Payment Successful!",
		html,
	)

	err = s.ticketRepository.UpdateTransactionStatus(receipt)
	if err != nil {
		return err
	}

	// Create notification
	err = s.ticketRepository.CreateNotification(&entity.Notification{
		ID:      uuid.New(),
		UserID:  receipt.UserID,
		Subject: "Pembayaran Sukses",
		IsRead:  false,
		Body:    "Pembayaran Anda sudah dikonfirmasi. Terima kasih telah memilih Depublic",
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *ticketService) GetReceiptByID(receiptID string) (*entity.GetReceiptByID, error) {
	return s.ticketRepository.GetReceiptByID(receiptID)
}

func (s *ticketService) GetReceiptHistory(userID string) ([]entity.Receipt, error) {
	return s.ticketRepository.GetReceiptHistory(userID)
}

func (s *ticketService) GetTicketsByUserID(userID string) ([]entity.Ticket, error) {
	return s.ticketRepository.GetTicketsByUserID(userID)
}
