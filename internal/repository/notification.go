package repository

import (
	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gorm.io/gorm"
)

type NotificationRepository interface {
	FindAllByUserID(id uuid.UUID) ([]entity.Notification, error)
	FindNotificationByID(id uuid.UUID) (*entity.Notification, error)
	UpdateNotification(id uuid.UUID, data *entity.Notification) (*entity.Notification, error)
}

type notificationRepository struct {
	db        *gorm.DB
	cacheable cache.Cacheable
}

func NewNotificationRepository(db *gorm.DB, cacheable cache.Cacheable) NotificationRepository {
	return &notificationRepository{db: db, cacheable: cacheable}
}

func (n *notificationRepository) FindAllByUserID(id uuid.UUID) ([]entity.Notification, error) {
	var notification []entity.Notification

	if err := n.db.Preload("User").Where("user_id = ?", id).Find(&notification).Error; err != nil {
		return nil, err
	}

	return notification, nil
}

func (n *notificationRepository) FindNotificationByID(id uuid.UUID) (*entity.Notification, error) {

	notification := new(entity.Notification)

	if err := n.db.Where("id = ?", id).Take(&notification).Error; err != nil {
		return notification, err
	}

	return notification, nil
}

func (n *notificationRepository) UpdateNotification(id uuid.UUID, data *entity.Notification) (*entity.Notification, error) {

	notification := new(entity.Notification)

	if err := n.db.Model(&notification).Where("id = ?", id).Updates(data).Error; err != nil {
		return notification, err
	}

	return notification, nil

}
