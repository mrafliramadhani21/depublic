package repository

import (
	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gorm.io/gorm"
)

type TicketRepository interface {
	CreateReceipt(receipt *entity.Receipt) (*entity.Receipt, error)
	CreateTicket(ticket *entity.Ticket) error
	GetCategoryByID(categoryID uuid.UUID) (*entity.Category, error)
	GetUser(userId uuid.UUID) (*entity.User, error)
	GetTicketsByUserID(userID string) ([]entity.Ticket, error)
	GetReceiptByID(receiptID string) (*entity.GetReceiptByID, error)
	GetReceiptHistory(userID string) ([]entity.Receipt, error)
	UpdateTransactionStatus(data *entity.Receipt) error
	UpdateCategoriesStock(categoryID uuid.UUID, quantity int64) error
	CreateNotification(data *entity.Notification) error
	UpdateNotification(id uuid.UUID, data *entity.Notification) error
}

type ticketRepository struct {
	db        *gorm.DB
	cacheable cache.Cacheable
}

func NewTicketRepository(db *gorm.DB, cacheable cache.Cacheable) TicketRepository {
	return &ticketRepository{db: db, cacheable: cacheable}
}

func (r *ticketRepository) CreateReceipt(receipt *entity.Receipt) (*entity.Receipt, error) {
	if err := r.db.Create(&receipt).Error; err != nil {
		return receipt, err
	}
	return receipt, nil
}

func (r *ticketRepository) CreateTicket(ticket *entity.Ticket) error {
	return r.db.Create(&ticket).Error
}

func (r *ticketRepository) GetCategoryByID(categoryID uuid.UUID) (*entity.Category, error) {
	var category *entity.Category
	err := r.db.Table("categories").Where("id = ?", categoryID).Select("price, name, stock").Scan(&category).Error
	return category, err
}

func (r *ticketRepository) GetTicketsByUserID(userID string) ([]entity.Ticket, error) {
	var tickets []entity.Ticket
	err := r.db.Table("tickets").Where("user_id = ?", userID).Find(&tickets).Error
	return tickets, err
}

func (r *ticketRepository) GetUser(userId uuid.UUID) (*entity.User, error) {
	var user *entity.User
	err := r.db.Preload("Receipts").Where("users.id = ?", userId).Find(&user).Error
	return user, err
}

func (r *ticketRepository) GetReceiptByID(receiptID string) (*entity.GetReceiptByID, error) {
	var receipt *entity.GetReceiptByID
	err := r.db.Table("receipts").Where("id = ?", receiptID).Scan(&receipt).Error
	return receipt, err
}

func (r *ticketRepository) GetReceiptHistory(userID string) ([]entity.Receipt, error) {
	var receipts []entity.Receipt
	err := r.db.Table("receipts").Where("user_id = ?", userID).Scan(&receipts).Error
	return receipts, err
}

func (r *ticketRepository) UpdateTransactionStatus(data *entity.Receipt) error {
	err := r.db.Table("receipts").Where("id = ?", data.ID).Updates(data).Error

	if err != nil {
		return err
	}
	return nil
}

func (r *ticketRepository) UpdateCategoriesStock(categoryID uuid.UUID, quantity int64) error {
	err := r.db.Table("categories").Where("id = ?", categoryID).Update("stock", gorm.Expr("stock - ?", quantity)).Error
	return err
}

func (r *ticketRepository) CreateNotification(data *entity.Notification) error {
	if err := r.db.Create(&data).Error; err != nil {
		return err
	}
	return nil
}

func (r *ticketRepository) UpdateNotification(id uuid.UUID, data *entity.Notification) error {
	notification := new(entity.Notification)

	if err := r.db.Model(&notification).Where("id = ?", id).Updates(data).Error; err != nil {
		return err
	}
	return nil
}
