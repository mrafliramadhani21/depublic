package repository

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gorm.io/gorm"
)

type UserRepository interface {
	FindUserByID(id uuid.UUID) (*entity.User, error)
	FindUserByEmail(email string) (*entity.User, error)
	FindAllUser() ([]entity.User, error)
	CreateUser(user *entity.User) (*entity.User, error)
	UpdateUser(user *entity.User) (*entity.User, error)
	UpdateVerificationStatus(user *entity.User) (*entity.User, error)
	UpdateProfile(user *entity.User) (*entity.User, error)
	DeleteUser(user *entity.User) (bool, error)
}

type userRepository struct {
	db        *gorm.DB
	cacheable cache.Cacheable
}

func NewUserRepository(db *gorm.DB, cacheable cache.Cacheable) UserRepository {
	return &userRepository{db: db, cacheable: cacheable}
}

func (r *userRepository) FindUserByID(id uuid.UUID) (*entity.User, error) {
	user := new(entity.User)

	if err := r.db.Where("users.id = ?", id).
		Take(user).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) FindUserByEmail(email string) (*entity.User, error) {
	user := new(entity.User)
	if err := r.db.Where("email = ?", email).Take(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

func (r *userRepository) FindAllUser() ([]entity.User, error) {
	users := make([]entity.User, 0)

	key := "FindAllUsers"

	data := r.cacheable.Get(key)
	if data == "" {
		if err := r.db.Find(&users).Error; err != nil {
			return users, err
		}
		marshalledUsers, _ := json.Marshal(users)
		err := r.cacheable.Set(key, marshalledUsers, 5*time.Minute)
		if err != nil {
			return users, err
		}
	} else {
		// Data ditemukan di Redis, unmarshal data ke users
		err := json.Unmarshal([]byte(data), &users)
		if err != nil {
			return users, err
		}
	}

	return users, nil
}

func (r *userRepository) CreateUser(user *entity.User) (*entity.User, error) {
	if err := r.db.Create(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

func (r *userRepository) UpdateUser(user *entity.User) (*entity.User, error) {
	// Use map to store fields to be updated.
	fields := make(map[string]interface{})

	// Update fields only if they are not empty.
	if user.Email != "" {
		fields["email"] = user.Email
	}
	if user.Password != "" {
		fields["password"] = user.Password
	}

	// Update the database in one query.
	if err := r.db.Model(user).Where("id = ?", user.ID).Updates(fields).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) UpdateVerificationStatus(user *entity.User) (*entity.User, error) {
	if user.IsVerified {
		return user, errors.New("email sudah diverifikasi")
	}

	if err := r.db.Model(user).Where("id = ?", user.ID).Update("is_verified", true).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) UpdateProfile(user *entity.User) (*entity.User, error) {
	fields := make(map[string]interface{})
	if user.Fullname != "" {
		fields["fullname"] = user.Fullname
	}
	if user.Email != "" {
		fields["email"] = user.Email
	}
	if user.Password != "" {
		fields["password"] = user.Password
	}
	if user.ProfilePicture != "" {
		fields["profile_picture"] = user.ProfilePicture
	}
	if user.PhoneNumber != "" {
		fields["phone_number"] = user.PhoneNumber
	}

	if err := r.db.Model(user).Where("id = ?", user.ID).Updates(fields).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r *userRepository) DeleteUser(user *entity.User) (bool, error) {
	if err := r.db.Delete(&user).Error; err != nil {
		return false, err
	}
	return true, nil
}
