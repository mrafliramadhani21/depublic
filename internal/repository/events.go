package repository

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gorm.io/gorm"
)

type EventRepository interface {
	// Existing methods
	GetEventCategories(page, pageSize int) ([]entity.Category, error)
	BuyTicket(ticket *entity.Ticket) (*entity.Ticket, error)

	GetSearchEvent(name, location string) ([]entity.Event, error)
	GetEventsByDateRange(start, end time.Time) ([]entity.Event, error)
	GetEventsByPriceRange(minPrice, maxPrice float64) ([]entity.Event, error)
	// GetFilterEvent(dateRangeStart, dateRangeEnd string, minPrice, maxPrice float64) ([]entity.Category, error)

	// New CRUD methods for CMS
	CreateEvent(event *entity.Event) (*entity.Event, error)
	GetEvents(page, pageSize int) ([]entity.Event, error)
	FindEventByID(eventID uuid.UUID) (*entity.Event, error)
	UpdateEvent(event *entity.Event) (*entity.Event, error)
	DeleteEvent(eventID uuid.UUID) error
	CreateCategory(category *entity.Category) (*entity.Category, error)
	GetCategories(page, pageSize int) ([]entity.Category, error)
	FindCategoriesById(categoryID uuid.UUID) (*entity.Category, error)
	FindCategoriesByEventId(eventID uuid.UUID, categoryName string) (*entity.Category, error)
	UpdateCategory(category *entity.Category) (*entity.Category, error)
	DeleteCategory(categoryID uuid.UUID) error
}

type eventRepository struct {
	db        *gorm.DB
	cacheable cache.Cacheable
}

func (r *eventRepository) GetEventCategories(page, pageSize int) ([]entity.Category, error) {
	var categories []entity.Category
	offset := (page - 1) * pageSize
	if err := r.db.Preload("Event").Limit(pageSize).Offset(offset).Find(&categories).Error; err != nil {
		return nil, err
	}
	return categories, nil
}

func (r *eventRepository) GetSearchEvent(name, location string) ([]entity.Event, error) {
	var events []entity.Event
	query := r.db

	if name != "" {
		query = query.Where("LOWER(name) LIKE ?", "%"+name+"%")
	}
	if location != "" {
		query = query.Where("LOWER(location) LIKE ?", "%"+location+"%")
	}

	if err := query.Find(&events).Error; err != nil {
		return nil, err
	}

	return events, nil
}

func (r *eventRepository) GetEventsByDateRange(start, end time.Time) ([]entity.Event, error) {
	var events []entity.Event
	err := r.db.Where("start_date BETWEEN ? AND ?", start, end).Find(&events).Error
	if err != nil {
		return nil, err
	}
	return events, nil
}

func (r *eventRepository) GetEventsByPriceRange(minPrice, maxPrice float64) ([]entity.Event, error) {
	var events []entity.Event
	err := r.db.Joins("JOIN categories ON categories.event_id = events.id").
		Where("categories.price BETWEEN ? AND ?", minPrice, maxPrice).
		Find(&events).Error
	if err != nil {
		return nil, err
	}
	return events, nil
}

func (r *eventRepository) BuyTicket(ticket *entity.Ticket) (*entity.Ticket, error) {
	if err := r.db.Create(ticket).Error; err != nil {
		return nil, err
	}
	return ticket, nil
}

func NewEventRepository(db *gorm.DB, cacheable cache.Cacheable) EventRepository {
	return &eventRepository{db: db, cacheable: cacheable}
}

// Existing methods implementation...

func (r *eventRepository) CreateEvent(event *entity.Event) (*entity.Event, error) {
	if err := r.db.Create(event).Error; err != nil {
		return nil, err
	}
	return event, nil
}

func (r *eventRepository) GetEvents(page, pageSize int) ([]entity.Event, error) {
	events := make([]entity.Event, 0)
	offset := (page - 1) * pageSize
	if err := r.db.Limit(pageSize).Offset(offset).Find(&events).Error; err != nil {
		return nil, err
	}
	return events, nil
}

func (r *eventRepository) FindEventByID(eventID uuid.UUID) (*entity.Event, error) {
	event := new(entity.Event)
	if err := r.db.Where("id = ?", eventID).Take(event).Error; err != nil {
		return nil, err
	}
	return event, nil
}

func (r *eventRepository) UpdateEvent(event *entity.Event) (*entity.Event, error) {
	fields := make(map[string]interface{})

	if event.Name != "" {
		fields["name"] = event.Name
	}
	if event.Description != "" {
		fields["description"] = event.Description
	}
	if !event.Date.IsZero() {
		fields["date"] = event.Date
	}
	if event.Image != "" {
		fields["image"] = event.Image
	}
	if event.Location != "" {
		fields["location"] = event.Location
	}
	if !event.StartDate.IsZero() {
		fields["start_date"] = event.StartDate
	}

	if err := r.db.Model(event).Where("id = ?", event.ID).Updates(fields).Error; err != nil {
		return nil, err
	}
	return event, nil
}

func (r *eventRepository) DeleteEvent(eventID uuid.UUID) error {
	if err := r.db.Delete(&entity.Event{ID: eventID}).Error; err != nil {
		return err
	}
	return nil
}

func (r *eventRepository) CreateCategory(category *entity.Category) (*entity.Category, error) {
	if err := r.db.Create(category).Error; err != nil {
		return nil, err
	}
	return category, nil
}

func (r *eventRepository) GetCategories(page, pageSize int) ([]entity.Category, error) {
	categories := make([]entity.Category, 0)
	offset := (page - 1) * pageSize
	if err := r.db.Limit(pageSize).Offset(offset).Find(&categories).Error; err != nil {
		return nil, err
	}
	return categories, nil
}

func (r *eventRepository) FindCategoriesById(categoryID uuid.UUID) (*entity.Category, error) {
	category := new(entity.Category)
	if err := r.db.Where("id = ?", categoryID).Take(category).Error; err != nil {
		return nil, err
	}
	return category, nil
}

func (r *eventRepository) FindCategoriesByEventId(eventID uuid.UUID, categoryName string) (*entity.Category, error) {
	category := new(entity.Category)
	if err := r.db.Where("event_id = ?", eventID).Where("name = ?", categoryName).Take(category).Error; err != nil {
		return nil, err
	}
	return category, nil
}

func (r *eventRepository) UpdateCategory(category *entity.Category) (*entity.Category, error) {
	if err := r.db.Save(category).Error; err != nil {
		return nil, err
	}
	return category, nil
}

func (r *eventRepository) DeleteCategory(categoryID uuid.UUID) error {
	if err := r.db.Delete(&entity.Category{ID: categoryID}).Error; err != nil {
		return err
	}
	return nil
}
