package router

import (
	"net/http"

	"gitlab.com/mrafliramadhani21/depublic/internal/http/handler"
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
)

func ProfilePrivateRoutes(profileHandler handler.ProfileHandler) []*route.Route {
	return []*route.Route{
		{
			Method:  http.MethodGet,
			Path:    "/profile/:id",
			Handler: profileHandler.FindUserByID,
		},
		{
			Method:  http.MethodPut,
			Path:    "/profile/:id",
			Handler: profileHandler.UpdateProfile,
		},
	}
}
