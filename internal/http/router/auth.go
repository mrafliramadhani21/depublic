package router

import (
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
)

func AuthPublicRoutes() []*route.Route {
	return []*route.Route{}
}

func AuthPrivateRoutes() []*route.Route {
	return nil
}
