package router

import (
	"net/http"

	"gitlab.com/mrafliramadhani21/depublic/internal/http/handler"
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
)

func EventPublicRoutes(eventHandler handler.EventHandler) []*route.Route {
	return []*route.Route{
		{
			Method:  http.MethodGet,
			Path:    "/events/:event_id/categories",
			Handler: eventHandler.GetEventCategories,
		},
		// {
		// 	Method:  http.MethodPost,
		// 	Path:    "/events/buy",
		// 	Handler: eventHandler.BuyTicket,
		// },
	}
}

func EventPrivateRoutes(eventHandler handler.EventHandler) []*route.Route {
	return []*route.Route{
		{
			Method:  http.MethodPost,
			Path:    "/cms/events",
			Handler: eventHandler.CreateEvent,
		},
		{
			Method:  http.MethodPut,
			Path:    "/cms/events/:id",
			Handler: eventHandler.UpdateEvent,
		},
		{
			Method:  http.MethodDelete,
			Path:    "/cms/events/:id",
			Handler: eventHandler.DeleteEvent,
		},
		{
			Method:  http.MethodPost,
			Path:    "/cms/categories",
			Handler: eventHandler.CreateCategory,
		},
		{

			Method:  http.MethodPut,
			Path:    "/cms/categories/:id",
			Handler: eventHandler.UpdateCategory,
		},
		{
			Method:  http.MethodDelete,
			Path:    "/cms/categories/:id",
			Handler: eventHandler.DeleteCategory,
		},
	}
}
