package router

import (
	"net/http"

	"gitlab.com/mrafliramadhani21/depublic/internal/http/handler"
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
)

const (
	Superadmin = "Superadmin"
	Admin      = "Admin"
	Buyer      = "Buyer"
)

var (
	allRoles       = []string{Admin, Buyer, Superadmin}
	onlySuperadmin = []string{Superadmin}
	onlyAdmin      = []string{Admin, Superadmin}
	onlyBuyer      = []string{Buyer}
)

func AppPublicRoutes(appHandler handler.AppHandler) []*route.Route {
	return []*route.Route{
		{
			Method:  http.MethodPost,
			Path:    "/login",
			Handler: appHandler.UserHandler.Login,
		},
		{
			Method:  http.MethodPost,
			Path:    "/register",
			Handler: appHandler.UserHandler.Register,
		},
		{
			Method:  http.MethodGet,
			Path:    "/events",
			Handler: appHandler.EventHandler.GetEventCategories,
		},
		{
			Method:  http.MethodGet,
			Path:    "/events/search",
			Handler: appHandler.EventHandler.GetSearchEvent,
		},
		{
			Method:  http.MethodGet,
			Path:    "/events/filterdate",
			Handler: appHandler.EventHandler.GetEventsByDateRange,
		},
		{
			Method:  http.MethodGet,
			Path:    "/events/filterprice",
			Handler: appHandler.EventHandler.GetEventsByPriceRange,
		},
		{
			Method:  http.MethodGet,
			Path:    "/verify/:id",
			Handler: appHandler.UserHandler.VerifyEmail,
		},
		{
			Method:  http.MethodPost,
			Path:    "/payment/webhook",
			Handler: appHandler.TicketHandler.WebhookMidtrans,
		},
	}
}

func AppPrivateRoutes(appHandler handler.AppHandler) []*route.Route {
	return []*route.Route{
		{
			Method:  http.MethodPost,
			Path:    "/cms/user",
			Handler: appHandler.UserHandler.CreateUser,
			Roles:   onlySuperadmin,
		},
		{
			Method:  http.MethodGet,
			Path:    "/profile/:id",
			Handler: appHandler.ProfileHandler.FindUserByID,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodPut,
			Path:    "/cms/profile/:id",
			Handler: appHandler.ProfileHandler.UpdateProfile,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodPost,
			Path:    "/cms/events",
			Handler: appHandler.EventHandler.CreateEvent,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodGet,
			Path:    "/cms/events",
			Handler: appHandler.EventHandler.GetEvents,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodGet,
			Path:    "/cms/events/:id",
			Handler: appHandler.EventHandler.FindEventByID,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodPut,
			Path:    "/cms/events/:id",
			Handler: appHandler.EventHandler.UpdateEvent,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodDelete,
			Path:    "/cms/events/:id",
			Handler: appHandler.EventHandler.DeleteEvent,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodPost,
			Path:    "/cms/categories",
			Handler: appHandler.EventHandler.CreateCategory,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodGet,
			Path:    "/cms/categories",
			Handler: appHandler.EventHandler.GetCategories,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodGet,
			Path:    "/cms/categories/:id",
			Handler: appHandler.EventHandler.FindCategoriesByID,
			Roles:   allRoles,
		},
		{

			Method:  http.MethodPut,
			Path:    "/cms/categories/:id",
			Handler: appHandler.EventHandler.UpdateCategory,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodDelete,
			Path:    "/cms/categories/:id",
			Handler: appHandler.EventHandler.DeleteCategory,
			Roles:   onlyAdmin,
		},
		{
			Method:  http.MethodPost,
			Path:    "/receipt",
			Handler: appHandler.TicketHandler.CreateReceipt,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodGet,
			Path:    "/receipt/history",
			Handler: appHandler.TicketHandler.GetReceiptHistory,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodGet,
			Path:    "/receipt/ticket/:id",
			Handler: appHandler.TicketHandler.GetTicket,
			Roles:   allRoles,
		},
		{
			Method:  http.MethodGet,
			Path:    "/notification",
			Handler: appHandler.NotificationHandler.ListNotification,
			Roles:   onlyBuyer,
		},
	}
}
