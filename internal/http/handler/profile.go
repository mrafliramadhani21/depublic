package handler

import (
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/http/binder"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/response"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
)

type ProfileHandler struct {
	profileService service.ProfileService
}

func NewProfileHandler(profileService service.ProfileService) ProfileHandler {
	return ProfileHandler{profileService: profileService}
}

func (h *ProfileHandler) FindUserByID(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userId := claims.ID

	profile, err := h.profileService.FindUserByID(userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses menampilkan data user", profile))
}

func (h *ProfileHandler) UpdateProfile(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userId := claims.ID

	input := binder.UpdateProfileUser{
		Fullname:    c.FormValue("fullname"),
		Email:       c.FormValue("email"),
		PhoneNumber: c.FormValue("phone_number"),
		Password:    c.FormValue("password"),
	}

	file, err := c.FormFile("profile_picture")
	if err != nil && err != http.ErrMissingFile {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "failed to upload image"))
	}

	var profilePicturePath string
	if file != nil {
		src, err := file.Open()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to open image"))
		}
		defer src.Close()

		// Create images directory if not exists
		imageDir := "images"
		if _, err := os.Stat(imageDir); os.IsNotExist(err) {
			err := os.Mkdir(imageDir, os.ModePerm)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image directory"))
			}
		}

		// Create a new file with a unique name in the images directory
		ext := filepath.Ext(file.Filename)
		newFilename := uuid.New().String() + ext
		profilePicturePath = filepath.Join(imageDir, newFilename)

		dst, err := os.Create(profilePicturePath)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image file"))
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to save image file"))
		}
		input.ProfilePicture = profilePicturePath
	}

	id := uuid.MustParse(userId)

	updateUser := entity.UpdateProfile(id, input.Fullname, input.Email, input.ProfilePicture, input.PhoneNumber, input.Password)

	profile, err := h.profileService.UpdateProfile(updateUser)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "update user success", profile))
}
