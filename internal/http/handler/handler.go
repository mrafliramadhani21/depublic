package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/mrafliramadhani21/depublic/pkg/response"
	"gitlab.com/mrafliramadhani21/depublic/pkg/validator"
)

type AppHandler struct {
	WelcomeHandler      echo.HandlerFunc
	UserHandler         UserHandler
	ProfileHandler      ProfileHandler
	EventHandler        EventHandler
	TicketHandler       TicketHandler
	NotificationHandler NotificationHandler
}

func NewAppPublicHandler(
	userHandler UserHandler,
	profileHandler ProfileHandler,
	eventHandler EventHandler,
	ticketHandler TicketHandler) AppHandler {
	return AppHandler{
		WelcomeHandler: welcome,
		UserHandler:    userHandler,
		ProfileHandler: profileHandler,
		EventHandler:   eventHandler,
		TicketHandler:  ticketHandler,
	}
}

func NewAppPrivateHandler(
	userHandler UserHandler,
	profileHandler ProfileHandler,
	eventHandler EventHandler,
	ticketHandler TicketHandler,
	notificationHandler NotificationHandler) AppHandler {
	return AppHandler{
		WelcomeHandler:      welcome,
		UserHandler:         userHandler,
		ProfileHandler:      profileHandler,
		EventHandler:        eventHandler,
		TicketHandler:       ticketHandler,
		NotificationHandler: notificationHandler,
	}
}

func welcome(c echo.Context) error {
	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "Depublic: App Ticketing", nil))
}

func checkValidation(input interface{}) (errorMessage string, data interface{}) {
	validationErrors := validator.Validate(input)
	if validationErrors != nil {
		if _, exists := validationErrors["error"]; exists {
			return "validasi input gagal", nil
		}
		return "validasi input gagal", validationErrors
	}
	return "", nil
}
