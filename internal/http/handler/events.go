package handler

import (
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/http/binder"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/response"
)

type EventHandler struct {
	eventService service.EventService
}

func NewEventHandler(eventService service.EventService) EventHandler {
	return EventHandler{eventService: eventService}
}

func (h *EventHandler) GetEventCategories(c echo.Context) error {
	pageParam := c.QueryParam("page")
	pageSizeParam := c.QueryParam("pageSize")

	page, err := strconv.Atoi(pageParam)
	if err != nil || page < 1 {
		page = 1
	}

	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil || pageSize < 1 {
		pageSize = 10
	}

	categories, err := h.eventService.GetEventCategories(page, pageSize)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to get categories"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", categories))
}

func (h *EventHandler) GetSearchEvent(c echo.Context) error {
	name := strings.ToLower(c.FormValue("name"))
	location := strings.ToLower(c.FormValue("location"))

	if name == "" && location == "" {
		return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", []entity.Event{}))
	}

	events, err := h.eventService.GetSearchEvent(name, location)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to search events"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", events))
}

func (h *EventHandler) GetEventsByDateRange(c echo.Context) error {
	startDate := c.FormValue("startDate")
	endDate := c.FormValue("endDate")

	// Validate the dates
	if startDate == "" || endDate == "" {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "startDate and endDate are required"))
	}

	start, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid startDate format"))
	}

	end, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid endDate format"))
	}

	if start.After(end) {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "startDate cannot be after endDate"))
	}

	events, err := h.eventService.GetEventsByDateRange(start, end)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to search events"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", events))
}

func (h *EventHandler) GetEventsByPriceRange(c echo.Context) error {
	minPriceStr := c.FormValue("minPrice")
	maxPriceStr := c.FormValue("maxPrice")

	// Validate the prices
	if minPriceStr == "" || maxPriceStr == "" {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "minPrice and maxPrice are required"))
	}

	minPrice, err := strconv.ParseFloat(minPriceStr, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid minPrice format"))
	}

	maxPrice, err := strconv.ParseFloat(maxPriceStr, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid maxPrice format"))
	}

	if minPrice > maxPrice {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "minPrice cannot be greater than maxPrice"))
	}

	events, err := h.eventService.GetEventsByPriceRange(minPrice, maxPrice)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to search events"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", events))
}

func (h *EventHandler) CreateEvent(c echo.Context) error {
	input := binder.CreateEventRequest{
		Name:        c.FormValue("name"),
		Description: c.FormValue("description"),
		Date:        c.FormValue("date"),
		Image:       c.FormValue("image"),
		Location:    c.FormValue("location"),
		StartDate:   c.FormValue("start_date"),
	}

	file, err := c.FormFile("image")
	if err != nil && err != http.ErrMissingFile {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "failed to upload image"))
	}

	var imageEvents string
	if file != nil {
		src, err := file.Open()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to open image"))
		}
		defer src.Close()

		// Create images directory if not exists
		imageDir := "images/events-images"
		if _, err := os.Stat(imageDir); os.IsNotExist(err) {
			err := os.Mkdir(imageDir, os.ModePerm)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image directory"))
			}
		}

		// Create a new file with a unique name in the images directory
		ext := filepath.Ext(file.Filename)
		newFilename := uuid.New().String() + ext
		imageEvents = filepath.Join(imageDir, newFilename)

		dst, err := os.Create(imageEvents)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image file"))
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to save image file"))
		}
		input.Image = imageEvents
	}

	layoutTime := "2006-01-02 15:04:05"

	// Convert date and startDate from string to time.Time
	date, err := time.Parse(layoutTime, input.Date)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid date format"))
	}

	startDate, err := time.Parse(layoutTime, input.StartDate)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid start date format"))
	}

	eventID := uuid.New()

	newEvent := entity.NewEvent(eventID, input.Name, input.Description, input.Location, input.Image, startDate, date)

	if errorMessage, data := checkValidation(input); errorMessage != "" {
		return c.JSON(http.StatusBadRequest, response.SuccessResponse(http.StatusBadRequest, errorMessage, data))
	}

	event, err := h.eventService.CreateEvent(newEvent)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses membuat event baru", event))
}

func (h *EventHandler) GetEvents(c echo.Context) error {
	pageParam := c.QueryParam("page")
	pageSizeParam := c.QueryParam("pageSize")

	page, err := strconv.Atoi(pageParam)
	if err != nil || page < 1 {
		page = 1
	}

	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil || pageSize < 1 {
		pageSize = 10
	}

	events, err := h.eventService.GetEvents(page, pageSize)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to get events"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", events))
}

func (h *EventHandler) FindEventByID(c echo.Context) error {
	EventID := c.Param("id")
	id, err := uuid.Parse(EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid event ID"))
	}

	event, err := h.eventService.FindEventByID(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", event))
}

func (h *EventHandler) UpdateEvent(c echo.Context) error {
	EventID := c.Param("id")
	id, err := uuid.Parse(EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid event ID"))
	}

	input := binder.UpdateEventRequest{
		Name:        c.FormValue("name"),
		Description: c.FormValue("description"),
		Date:        c.FormValue("date"),
		Image:       c.FormValue("image"),
		Location:    c.FormValue("location"),
		StartDate:   c.FormValue("start_date"),
	}

	file, err := c.FormFile("image")
	if err != nil && err != http.ErrMissingFile {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "failed to upload image"))
	}

	var profilePicturePath string
	if file != nil {
		src, err := file.Open()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to open image"))
		}
		defer src.Close()

		// Create images directory if not exists
		imageDir := "images/events-images"
		if _, err := os.Stat(imageDir); os.IsNotExist(err) {
			err := os.MkdirAll(imageDir, os.ModePerm)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image directory"))
			}
		}

		// Create a new file with a unique name in the images directory
		ext := filepath.Ext(file.Filename)
		newFilename := uuid.New().String() + ext
		profilePicturePath = filepath.Join(imageDir, newFilename)

		dst, err := os.Create(profilePicturePath)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to create image file"))
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to save image file"))
		}
		input.Image = profilePicturePath
	}

	// Convert date and startDate from string to time.Time if provided
	var date, startDate time.Time
	if input.Date != "" {
		date, err = time.Parse(time.RFC822, input.Date)
		if err != nil {
			return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid date format"))
		}
	}

	if input.StartDate != "" {
		startDate, err = time.Parse(time.RFC822, input.StartDate)
		if err != nil {
			return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid start date format"))
		}
	}

	updateEvent := entity.UpdateEvent(id, input.Name, input.Description, input.Location, input.Image, startDate, date)

	event, err := h.eventService.UpdateEvent(updateEvent)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses memperbarui event", event))
}

func (h *EventHandler) DeleteEvent(c echo.Context) error {
	EventID := c.Param("id")
	id, err := uuid.Parse(EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "Invalid event ID"))
	}

	if err := h.eventService.DeleteEvent(id); err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "event deleted successfully", nil))
}

func (h *EventHandler) CreateCategory(c echo.Context) error {
	req := binder.CreateCategoryRequest{}

	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid input"))
	}

	event_id, err := uuid.Parse(req.EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid event ID"))
	}

	if req.Price <= 0 {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "harap masukkan price"))
	}

	if req.Stock <= 0 {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "harap masukkan stock"))
	}

	categoryID := uuid.New()
	newCategory := entity.NewCategory(categoryID, event_id, req.Name, req.Price, req.Stock)

	if errorMessage, data := checkValidation(req); errorMessage != "" {
		return c.JSON(http.StatusBadRequest, response.SuccessResponse(http.StatusBadRequest, errorMessage, data))
	}

	createdCategory, err := h.eventService.CreateCategory(newCategory)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "category created successfully", createdCategory))
}

func (h *EventHandler) GetCategories(c echo.Context) error {
	pageParam := c.QueryParam("page")
	pageSizeParam := c.QueryParam("pageSize")

	page, err := strconv.Atoi(pageParam)
	if err != nil || page < 1 {
		page = 1
	}

	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil || pageSize < 1 {
		pageSize = 10
	}

	categories, err := h.eventService.GetCategories(page, pageSize)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to get categories"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", categories))
}

func (h *EventHandler) FindCategoriesByID(c echo.Context) error {
	eventID := c.Param("id")
	id, err := uuid.Parse(eventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid event ID"))
	}

	categories, err := h.eventService.FindCategoriesById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, "failed to get categories"))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "success", categories))
}

func (h *EventHandler) UpdateCategory(c echo.Context) error {
	categoryID := c.Param("id")
	id, err := uuid.Parse(categoryID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid category ID"))
	}

	var req binder.UpdateCategoryRequest
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid input"))
	}

	event_id, err := uuid.Parse(req.EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid event ID"))
	}

	newCategory := entity.UpdateCategory(id, event_id, req.Name, req.Price, req.Stock)

	updatedCategory, err := h.eventService.UpdateCategory(newCategory)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "category updated successfully", updatedCategory))
}

func (h *EventHandler) DeleteCategory(c echo.Context) error {
	categoryID := c.Param("id")
	id, err := uuid.Parse(categoryID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid category ID"))
	}
	if err := h.eventService.DeleteCategory(id); err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "category deleted successfully", nil))
}
