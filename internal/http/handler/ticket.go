package handler

import (
	"database/sql"
	"net/http"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/mrafliramadhani21/depublic/internal/entity"
	"gitlab.com/mrafliramadhani21/depublic/internal/http/binder"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/response"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
)

type TicketHandler struct {
	ticketService service.TicketService
}

func NewTicketHandler(ticketService service.TicketService) TicketHandler {
	return TicketHandler{ticketService: ticketService}
}

func (h *TicketHandler) CreateReceipt(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userId := claims.ID

	var req binder.CreateReceiptRequest
	err := c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid input"))
	}

	dateFormat := time.Now().Format("20060102")
	randomPart := uuid.New().String()[:8]
	id := dateFormat + "/" + randomPart + "/dpblc"

	eventID, err := uuid.Parse(req.EventID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid event ID"))
	}

	categoryID, err := uuid.Parse(req.CategoryID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, "invalid category ID"))
	}

	idUser := uuid.MustParse(userId)
	newReceipt := entity.NewReceipt(id, idUser, eventID, categoryID, req.Quantity, 0)

	receipt, err := h.ticketService.CreateReceipt(newReceipt)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses membuat transaksi", receipt))
}

func (h *TicketHandler) GetTicket(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userId := claims.ID

	tickets, err := h.ticketService.GetTicketsByUserID(userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}
	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses mendapatkan tiket", tickets))
}

func (h *TicketHandler) GetReceiptHistory(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userId := claims.ID

	receipts, err := h.ticketService.GetReceiptHistory(userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}
	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses mendapatkan riwayat transaksi", receipts))
}

func (h *TicketHandler) WebhookMidtrans(c echo.Context) error {
	var req binder.WebhookMidtransRequest
	err := c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.ErrorResponse(http.StatusInternalServerError, err.Error()))
	}

	if req.TransactionStatus != "settlement" {
		return c.JSON(http.StatusOK, nil)
	}

	floatGrossAmount, err := strconv.ParseFloat(req.GrossAmount, 64)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	paidAt, _ := time.Parse("2006-01-02 15:04:05", req.SettlementTime)

	receipt, err := h.ticketService.GetReceiptByID(req.OrderID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	newReceipt := &entity.Receipt{
		ID:           req.OrderID,
		UserID:       receipt.UserID,
		Payment:      int64(floatGrossAmount),
		PurchaseDate: sql.NullTime{Time: paidAt, Valid: true},
		Tickets:      []entity.Ticket{},
	}

	// Generate tickets
	for i := 0; i < int(receipt.Quantity); i++ {
		randomPart := uuid.New().String()[:8]
		id := randomPart + "/ticket"

		newTicket := entity.NewTicket(id, receipt.ID, receipt.UserID)
		newReceipt.Tickets = append(newReceipt.Tickets, newTicket)
	}

	err = h.ticketService.UpdateTransactionStatus(newReceipt)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, nil)
}
