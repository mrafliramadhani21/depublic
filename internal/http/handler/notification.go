package handler

import (
	"net/http"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/response"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
)

type NotificationHandler struct {
	notificationServices service.NotificationService
}

func NewNotificationHandler(notificationServices service.NotificationService) NotificationHandler {
	return NotificationHandler{
		notificationServices: notificationServices,
	}
}

func (h NotificationHandler) ListNotification(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(*token.JwtCustomClaims)
	userid := claims.ID
	id := uuid.MustParse(userid)

	getNotification, err := h.notificationServices.GetByUser(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response.ErrorResponse(http.StatusBadRequest, err.Error()))
	}

	return c.JSON(http.StatusOK, response.SuccessResponse(http.StatusOK, "sukses memuat notifikasi", getNotification))

}
