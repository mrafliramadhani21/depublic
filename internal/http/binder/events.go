package binder

type CreateEventRequest struct {
	Name        string `json:"name" validate:"required"`
	Description string `json:"description" validate:"required"`
	Date        string `json:"date" validate:"required"`
	Image       string `json:"image" validate:"required"`
	Location    string `json:"location" validate:"required"`
	StartDate   string `json:"start_date" validate:"required"`
}

type UpdateEventRequest struct {
	ID          string `param:"id" validate:"required"`
	Name        string `json:"name" validate:"required"`
	Description string `json:"description" validate:"required"`
	Date        string `json:"date" validate:"required"`
	Image       string `json:"image" validate:"required"`
	Location    string `json:"location" validate:"required"`
	StartDate   string `json:"start_date" validate:"required"`
}

type CreateCategoryRequest struct {
	EventID string  `json:"event_id" validate:"required"`
	Name    string  `json:"name" validate:"required"`
	Price   float64 `json:"price" validate:"required"`
	Stock   int     `json:"stock" validate:"required"`
}

type UpdateCategoryRequest struct {
	ID      string  `param:"id" validate:"required"`
	EventID string  `json:"event_id" validate:"required"`
	Name    string  `json:"name" validate:"required"`
	Price   float64 `json:"price" validate:"required"`
	Stock   int     `json:"stock" validate:"required"`
}

type DeleteEventRequest struct {
	ID string `param:"id" validate:"required"`
}

type DeleteCategoryRequest struct {
	ID string `param:"id" validate:"required"`
}
