package binder

type UserLoginRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type UserCreateRequest struct {
	Fullname    string `json:"fullname" validate:"required"`
	Email       string `json:"email" validate:"required,email"`
	Password    string `json:"password" validate:"required"`
	PhoneNumber string `json:"phone_number" validate:"required"`
	Role        string `json:"role_id" validate:"required,oneof=Admin Buyer"`
}

type UserRegisterRequest struct {
	Fullname    string `json:"fullname" validate:"required"`
	Email       string `json:"email" validate:"required,email"`
	Password    string `json:"password" validate:"required"`
	PhoneNumber string `json:"phone_number" validate:"required"`
}

type UpdateProfileUser struct {
	ID             string `param:"id" validate:"required"`
	Fullname       string `json:"fullname" validate:"required"`
	Email          string `json:"email" validate:"required,email"`
	ProfilePicture string `json:"profile_picture"`
	PhoneNumber    string `json:"phone_number" validate:"required"`
	Password       string `json:"password" validate:"required"`
}

type UserUpdateRequest struct {
	ID          string `param:"id" validate:"required"`
	Fullname    string `json:"fullname" validate:"required"`
	Email       string `json:"email" validate:"required,email"`
	Password    string `json:"password" validate:"required"`
	PhoneNumber string `json:"phone_number" validate:"required"`
	Role        string `json:"role_id" validate:"required,oneof=Admin Buyer"`
}

type UserDeleteRequest struct {
	ID string `param:"id" validate:"required"`
}

type UserFindByIDRequest struct {
	ID string `param:"id" validate:"required"`
}
