package binder

import "github.com/google/uuid"

type NotificationCreatRequets struct {
	UserID  uuid.UUID `json:"user_id"`
	Subject string    `json:"subject"`
	Body    string    `json:"body"`
}

type NotificationUpdateRequest struct {
	IsRead bool `json:"is_read"`
}
