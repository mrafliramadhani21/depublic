package binder

type CreateReceiptRequest struct {
	EventID      string `json:"event_id" validate:"required"`
	CategoryID   string `json:"category_id" validate:"required"`
	PurchaseDate string `json:"purchase_date"`
	Quantity     int64  `json:"quantity,string" validate:"required"`
}

type CreateTicketRequest struct {
	ReceiptID string `json:"receipt_id" validate:"required"`
}

type WebhookMidtransRequest struct {
	TransactionTime   string `json:"transaction_time"`
	TransactionStatus string `json:"transaction_status"`
	TransactionID     string `json:"transaction_id"`
	StatusMessage     string `json:"status_message"`
	StatusCode        string `json:"status_code"`
	SignatureKey      string `json:"signature_key"`
	SettlementTime    string `json:"settlement_time"`
	PaymentType       string `json:"payment_type"`
	OrderID           string `json:"order_id"`
	MerchantID        string `json:"merchant_id"`
	GrossAmount       string `json:"gross_amount"`
	FraudStatus       string `json:"fraud_status"`
	Currency          string `json:"currency"`
}
