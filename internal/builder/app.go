package builder

import (
	"github.com/redis/go-redis/v9"
	"gitlab.com/mrafliramadhani21/depublic/configs"
	"gitlab.com/mrafliramadhani21/depublic/internal/http/handler"
	"gitlab.com/mrafliramadhani21/depublic/internal/http/router"
	"gitlab.com/mrafliramadhani21/depublic/internal/repository"
	"gitlab.com/mrafliramadhani21/depublic/internal/service"
	"gitlab.com/mrafliramadhani21/depublic/pkg/cache"
	"gitlab.com/mrafliramadhani21/depublic/pkg/encrypt"
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
	"gitlab.com/mrafliramadhani21/depublic/pkg/token"
	"gorm.io/gorm"
)

func BuildAppPublicRoutes(db *gorm.DB, cfg *configs.Config, tokenUseCase token.TokenUseCase, encryptTool encrypt.EncryptTool) []*route.Route {
	userRepository := repository.NewUserRepository(db, nil)
	userService := service.NewUserService(userRepository, tokenUseCase, encryptTool)
	userHandler := handler.NewUserHandler(userService)

	profileService := service.NewProfileService(userRepository, encryptTool)
	profileHandler := handler.NewProfileHandler(profileService)

	eventRepository := repository.NewEventRepository(db, nil)
	eventService := service.NewEventService(eventRepository)
	eventHandler := handler.NewEventHandler(eventService)

	ticketRepository := repository.NewTicketRepository(db, nil)
	ticketService := service.NewTicketService(ticketRepository, cfg)
	ticketHandler := handler.NewTicketHandler(ticketService)

	appHandler := handler.NewAppPublicHandler(userHandler, profileHandler, eventHandler, ticketHandler)
	return router.AppPublicRoutes(appHandler)
}

func BuildAppPrivateRoutes(db *gorm.DB, cfg *configs.Config, redisDB *redis.Client, encryptTool encrypt.EncryptTool) []*route.Route {
	cacheable := cache.NewCacheable(redisDB)
	userRepository := repository.NewUserRepository(db, cacheable)
	userService := service.NewUserService(userRepository, nil, encryptTool)
	userHandler := handler.NewUserHandler(userService)

	profileService := service.NewProfileService(userRepository, encryptTool)
	profileHandler := handler.NewProfileHandler(profileService)

	eventRepository := repository.NewEventRepository(db, cacheable)
	eventService := service.NewEventService(eventRepository)
	eventHandler := handler.NewEventHandler(eventService)

	ticketRepository := repository.NewTicketRepository(db, cacheable)
	ticketService := service.NewTicketService(ticketRepository, cfg)
	ticketHandler := handler.NewTicketHandler(ticketService)

	notifRepository := repository.NewNotificationRepository(db, cacheable)
	notifServices := service.NewNotificationServices(notifRepository)
	notifHandler := handler.NewNotificationHandler(notifServices)

	appHandler := handler.NewAppPrivateHandler(userHandler, profileHandler, eventHandler, ticketHandler, notifHandler)

	return router.AppPrivateRoutes(appHandler)
}
