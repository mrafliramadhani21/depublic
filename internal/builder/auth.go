package builder

import (
	"gitlab.com/mrafliramadhani21/depublic/internal/http/router"
	"gitlab.com/mrafliramadhani21/depublic/pkg/route"
)

func BuildAuthPublicRoutes() []*route.Route {
	return router.AuthPublicRoutes()
}

func BuildAuthPrivateRoutes() []*route.Route {
	return router.AuthPrivateRoutes()
}
