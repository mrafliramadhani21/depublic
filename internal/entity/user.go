package entity

import "github.com/google/uuid"

type User struct {
	ID             uuid.UUID      `json:"id"`
	Fullname       string         `json:"fullname"`
	Email          string         `json:"email"`
	Password       string         `json:"-"`
	ProfilePicture string         `json:"profile_picture"`
	Role           string         `json:"role"`
	PhoneNumber    string         `json:"phone_number"`
	IsVerified     bool           `json:"is_verified"`
	Receipts       []Receipt      `json:"transactions" gorm:"foreignKey:UserID"`
	Notifications  []Notification `json:"notifications" gorm:"foreignKey:UserID"`

	Auditable
}

func NewUser(fullname, email, password, role, phoneNumber string) *User {
	return &User{
		ID:          uuid.New(),
		Fullname:    fullname,
		Email:       email,
		Password:    password,
		Role:        role,
		PhoneNumber: phoneNumber,
		Auditable:   NewAuditable(),
	}
}

func NewUserRegister(fullname, email, password, phoneNumber string) *User {
	return &User{
		ID:          uuid.New(),
		Fullname:    fullname,
		Email:       email,
		Password:    password,
		PhoneNumber: phoneNumber,
		Auditable:   NewAuditable(),
	}
}

func UpdateUser(id uuid.UUID, fullname, email, password, role, phoneNumber string) *User {
	return &User{
		ID:          id,
		Fullname:    fullname,
		Email:       email,
		Password:    password,
		Role:        role,
		PhoneNumber: phoneNumber,
		Auditable:   UpdateAuditable(),
	}
}

func UpdateProfile(id uuid.UUID, fullname, email, profilePicture, phoneNumber, password string) *User {
	return &User{
		ID:             id,
		Fullname:       fullname,
		Email:          email,
		ProfilePicture: profilePicture,
		PhoneNumber:    phoneNumber,
		Password:       password,
		Auditable:      UpdateAuditable(),
	}
}
