package entity

import (
	"database/sql"

	"github.com/google/uuid"
)

type Receipt struct {
	ID           string       `json:"id"`
	UserID       uuid.UUID    `json:"user_id"`
	EventID      uuid.UUID    `json:"event_id"`
	CategoryID   uuid.UUID    `json:"category_id"`
	PurchaseDate sql.NullTime `json:"purchase_date"`
	Payment      int64        `json:"payment"`
	Quantity     int64        `json:"quantity"`
	Price        float64      `json:"price"`
	Tickets      []Ticket     `json:"tickets"`
	Auditable
}

type GetReceiptByID struct {
	ID       string    `json:"id"`
	UserID   uuid.UUID `json:"user_id"`
	Quantity int64     `json:"quantity"`
}

type Ticket struct {
	ID        string    `json:"id"`
	UserID    uuid.UUID `json:"user_id"`
	ReceiptID string    `json:"receipt_id"`
	Auditable
}

func NewReceipt(id string, userID, eventID, categoryID uuid.UUID, quantity int64, price float64) *Receipt {
	return &Receipt{
		ID:         id,
		UserID:     userID,
		EventID:    eventID,
		CategoryID: categoryID,
		Quantity:   quantity,
		Price:      price,
		Auditable:  NewAuditable(),
	}
}

func NewTicket(id, receiptID string, userID uuid.UUID) Ticket {
	return Ticket{
		ID:        id,
		UserID:    userID,
		ReceiptID: receiptID,
		Auditable: NewAuditable(),
	}
}
