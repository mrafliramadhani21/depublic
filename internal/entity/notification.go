package entity

import (
	"github.com/google/uuid"
)

type Notification struct {
	ID      uuid.UUID `json:"id"`
	UserID  uuid.UUID `json:"user_id"`
	User    User      `gorm:"foreignKey:UserID"`
	Subject string    `json:"subject"`
	IsRead  bool      `json:"is_read"`
	Body    string    `json:"body"`

	Auditable
}

func NewNotification(UserID uuid.UUID, subject string, body string) *Notification {
	return &Notification{
		ID:        uuid.New(),
		UserID:    UserID,
		Subject:   subject,
		IsRead:    false,
		Body:      body,
		Auditable: NewAuditable(),
	}

}

func UpdateNotification(IsRead bool) *Notification {
	return &Notification{
		IsRead:    IsRead,
		Auditable: UpdateAuditable(),
	}
}
