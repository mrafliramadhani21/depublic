package entity

import (
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Date        time.Time `json:"date"`
	Image       string    `json:"image"`
	StartDate   time.Time `json:"start_date"`
	Location    string    `json:"location"`
	Auditable
}

type Category struct {
	ID      uuid.UUID `json:"id"`
	EventID uuid.UUID `json:"event_id"`
	Event   Event     `json:"event,omitempty" gorm:"foreignKey:EventID"`
	Name    string    `json:"name"`
	Price   float64   `json:"price"`
	Stock   int       `json:"stock"`
	Auditable
}

func NewEvent(id uuid.UUID, name, description, location, image string, startDate, date time.Time) *Event {
	return &Event{
		ID:          uuid.New(),
		Name:        name,
		Description: description,
		Location:    location,
		Image:       image,
		StartDate:   startDate,
		Date:        date,
		Auditable:   NewAuditable(),
	}
}

func UpdateEvent(id uuid.UUID, name, description, location, image string, startDate, date time.Time) *Event {
	return &Event{
		ID:          id,
		Name:        name,
		Description: description,
		Location:    location,
		Image:       image,
		StartDate:   startDate,
		Date:        date,
		Auditable:   UpdateAuditable(),
	}
}

func NewCategory(id, event_id uuid.UUID, name string, price float64, stock int) *Category {
	return &Category{
		ID:        uuid.New(),
		EventID:   event_id,
		Name:      name,
		Price:     price,
		Stock:     stock,
		Auditable: NewAuditable(),
	}
}

func UpdateCategory(id, event_id uuid.UUID, name string, price float64, stock int) *Category {
	return &Category{
		ID:        id,
		EventID:   event_id,
		Name:      name,
		Price:     price,
		Stock:     stock,
		Auditable: UpdateAuditable(),
	}
}
