package configs

import (
	"errors"

	"github.com/caarlos0/env/v10"
	"github.com/joho/godotenv"
)

type Config struct {
	Env            string         `env:"ENV" envDefault:"dev"`
	Port           string         `env:"PORT" envDefault:"8000"`
	Postgres       PostgresConfig `envPrefix:"POSTGRES_"`
	JWT            JwtConfig      `envPrefix:"JWT_"`
	Redis          RedisConfig    `envPrefix:"REDIS_"`
	Encrypt        EncryptConfig  `envPrefix:"ENCRYPT_"`
	SMTP           SMTPConfig     `envPrefix:"SMTP_"`
	MidtransConfig MidtransConfig `envPrefix:"MIDTRANS_"`
	Namespace      string         `env:"NAMESPACE" envDefault:"depublic"`
}

type SMTPConfig struct {
	Username string `env:"USER"`
	From     string `env:"FROM"`
	Host     string `env:"HOST"`
	Port     string `env:"PORT"`
	Password string `env:"PASSWORD"`
}

type EncryptConfig struct {
	SecretKey string `env:"SECRET_KEY"`
	IV        string `env:"IV"`
}

type RedisConfig struct {
	Host     string `env:"HOST" envDefault:"localhost"`
	Port     string `env:"PORT" envDefault:"6379"`
	Password string `env:"PASSWORD" envDefault:""`
}

type PostgresConfig struct {
	Host     string `env:"HOST" envDefault:"localhost"`
	Port     string `env:"PORT" envDefault:"5432"`
	User     string `env:"USER" envDefault:"postgres"`
	Password string `env:"PASSWORD" envDefault:"postgres"`
	Database string `env:"DATABASE" envDefault:"postgres"`
}

type JwtConfig struct {
	SecretKey string `env:"SECRET_KEY"`
	ExpiredIn int8   `env:"EXPIRED_IN" envDefault:"24"`
}

type MidtransConfig struct {
	ServerKey string `env:"SERVER_KEY"`
}

func LoadConfig(envFile string) (*Config, error) {
	err := godotenv.Load(envFile)
	if err != nil {
		return nil, errors.New("failed to load .env file")
	}

	cfg := new(Config)

	err = env.Parse(cfg)
	if err != nil {
		return nil, errors.New("failed to parse config file")
	}

	return cfg, nil
}
